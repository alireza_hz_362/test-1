 var canvas=document.getElementById("my-canvas")
 var  ctx=canvas.getContext("2d")
 var DEGREE=Math.PI/180
 var frames = 0;
 /*************************state ******************/
  var state={
  current:0,
  getReady:0,
  game:1,
  over:2,
}
/**************************click handler *********************************/

function clickHandler(){
   switch (state.current) {
    case  state.getReady:
      state.current=state.game
      break;
    case state.game:
      bird.flap()
      break;
    default:
      pipes.position=[]
      score.value=0
      bird.rotation=0
      state.current=state.getReady
      bird.speed=0
      break;
   }
}
/*********************click (Event) **************************************/
document.addEventListener("click",clickHandler)
document.addEventListener("keydown",function(e){
  if(e.keyCode=="32"){
    clickHandler();   
  }
})
 /****************************add image*****************************************/
 var sprite = new Image();
 sprite.src="image/Sprites.png"
 var spritesTwo =new Image();
 spritesTwo.src="image/Sprites2.png"
 /****************************  define a pipes *****************************************/
 var pipes={
  top:{
    sX:399,
    sY:150,
  },
  bottom:{
    sX:434,
    sY:150,
  },
  w:23,
  h:360,
  dx:3,
  gap:80,
  position:[],
  maxYPos:-150,

  update:function() {
    if(state.current!= state.game) return;
    if (frames%100 == 0) {
       this.position.push({
        x:canvas.width,
        y:this.maxYPos*(Math.random()+1)  
       })
    } 
    
    for (let i = 0; i < this.position.length; i++) {
      let p = this.position[i];
      p.x -= this.dx
      
      let bottomPipesPos = p.y +this.h + this.gap;
      
       if( bird.x + bird.radius > p.x && 
           bird.x - bird.radius < p.x +this.w && 
           bird.y + bird.radius > p.y &&
           bird.y - bird.radius<p.y +this.h  ){

        state.current =state.over

       }

       if( bird.x + bird.radius > p.x && 
           bird.x - bird.radius < p.x +this.w && 
           bird.y + bird.radius > bottomPipesPos &&
           bird.y - bird.radius< bottomPipesPos + this.h  ){

     state.current =state.over

    }
         


      if (p.x+ this.w <= 0) {

        this.position.shift()
        score.value +=1;
        score.best = Math.max(score.value , score.best)
        localStorage.setItem("best", score.best)
        
      }
    }
  },

  draw:function () {
    for (let i = 0; i < this.position.length; i++) {
      let p = this.position[i];
      let topYPos=p.y;
      let bottomYpos= p.y + this.h + this.gap;
      ctx.drawImage(spritesTwo,this.top.sX,this.top.sY,this.w,this.h,p.x,topYPos,this.w,this.h)
      ctx.drawImage(spritesTwo,this.bottom.sX,this.bottom.sY,this.w,this.h,p.x,bottomYpos,this.w,this.h)
  }
   

 }
}

 /****************************  define a background *****************************************/
 var bg={
   sX:0,
   sY:154,
   w:140,
   h:358,
   x:0,
   y:canvas.height-120,
   draw:function(){
     ctx.drawImage(sprite,this.sX,this.sY,this.w,this.h,this.x,this.y,this.w,this.h)
     ctx.drawImage(sprite,this.sX,this.sY,this.w,this.h,this.x +this.w,this.y,this.w,this.h)
     ctx.drawImage(sprite,this.sX,this.sY,this.w,this.h,this.x +this.w +this.w,this.y,this.w,this.h)
    }
  }
  /****************************  define a forground *****************************************/
  var fg={
    sX:292,
    sY:0,
    w:167,
    h:56,
    x:0,
    y:canvas.height-55,
    dx:3,
    draw:function(){
      ctx.drawImage(sprite,this.sX,this.sY,this.w,this.h,this.x,this.y,this.w,this.h)
      ctx.drawImage(sprite,this.sX,this.sY,this.w,this.h,this.x +this.w,this.y,this.w,this.h)
      
    },
    update:function(){
      if (state.current== state.game) {
        this.x =(this.x - this.dx) % (this.w/10)
        
      }
    }
  }
  /****************************  define a getready *****************************************/
  var getReady={
    sX:294,
    sY:58,
    w:95,
    h:26,
    x:canvas.width/2-95/2,
    y:80,
    draw:function(){
      if(state.current==state.getReady){
      ctx.drawImage(sprite,this.sX,this.sY,this.w,this.h,this.x,this.y,this.w,this.h)
    }
  }
  }
    /****************************  define a gameover *****************************************/
    var gameOver={
      sX:393,
      sY:58,
      w:100,
      h:23,
      x:canvas.width/2-100/2,
      y:80,
      draw:function(){
        if (state.over==state.current) {
          ctx.drawImage(sprite,this.sX,this.sY,this.w,this.h,this.x,this.y,this.w,this.h)
        }
      }
    }
  /****************************  define a bird *****************************************/
  var bird={
    animation:[
      {sX:112,sY:380},
      {sX:112,sY:406},
      {sX:112,sY:431},
      {sX:112,sY:406},
    ],
    w:20,
    h:13,
    x:50,
    y:160,
    /***********gravity for bird (value)**********/
    speed:0,
    gravity:0.25,
    jump:4.7,
    /****************Rotation*********************/
    rotation:0,
    /****************collision detection*********************/
    radius:9,
    animationIndex:0,
    
    draw:function(){
      ctx.save() 
      ctx.translate(this.x ,this.y)
      ctx.rotate(this.rotation)
      let bird=this.animation[this.animationIndex]
      ctx.drawImage(sprite,bird.sX,bird.sY,this.w,this.h,-this.w/2,-this.h/2,this.w,this.h)
      ctx.restore()
    },
    update: function(){
      /******wings animation*******/
     let period =state.current == state.getReady ? 10 : 5
     this.animationIndex += frames % period == 0 ? 1 : 0;
     this.animationIndex =this.animationIndex % this.animation.length
     /*******Gravity**********/
     if(state.current==state.getReady){
      this.y=160; //stay in place
     }else{ 
       this.speed+=this.gravity
       this.y+=this.speed
       if(this.speed<this.jump){
          this.rotation =-25*DEGREE;
      }else{
         this.rotation =90*DEGREE;
      }
     }
    //*********************bird landing ********************  
     if(this.y + this.h/2 >= canvas.height - fg.h){
       this.y = canvas.height-fg.h -this.h/2
       this.animationIndex=1
         if(state.current==state.game){
           state.current=state.over
          }
        }
        
      },
      flap:function(){
        this.speed = -this.jump; 
      }
    }
    /****************************  define a score *****************************************/
    var score={
      best:parseInt(localStorage.getItem("best"))||0,
      value:0,


      draw:function() {
        ctx.fillStyle ="#fff"
        ctx.strokeStyle ="#000"

        if(state.current == state.game){
          ctx.lineWidth=2;
          ctx.font ="35px IMPACT"

          ctx.fillText( this.value, canvas.width/2, 50 )
          ctx.strokeText( this.value, canvas.width/2, 50 )
        }
        else if(state.current == state.over){ 
          ctx.lineWidth=2;
          ctx.font ="35px IMPACT"

          ctx.fillText( this.value,255, 186 )
          ctx.strokeText( this.value, 255, 186 )
          
          ctx.fillText( this.best, 255, 228 )
          ctx.strokeText( this.best, 255, 228 )

        }
        
      }
    }
    
    
    
    

    /****************************primary *****************************************/
  
  function update(){
   bird.update()
   fg.update()
   pipes.update()
 }
 
 function draw(){
   ctx.fillStyle="#54C0C9"
   ctx.fillRect(0,0,canvas.width,canvas.height)
   bg.draw()
   pipes.draw()
   fg.draw()
   bird.draw()
   getReady.draw()
   gameOver.draw()
   score.draw()
 }

 function animate(){
    requestAnimationFrame(animate)
    update()
    draw()
    frames ++;
}

 animate()